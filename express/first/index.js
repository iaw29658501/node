var express = require('express')
var app = express();

app.use(express.static('public'))

app.get('/', function (req, res) {
    res.send('welcome to home');
})


app.get('/help', function (req, res) {
    res.sendFile(__dirname + "/public/readme.txt");
})

var server = app.listen(8080, function () {
    console.log("app listening")
 })