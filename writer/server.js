var http = require('http')
var dt = require('./modulito.js')

http.createServer(function (req,resp) {
    resp.writeHead(200, {'Content-Type': 'text/html'});
    resp.write("Now is : " + dt.now());
    resp.end();
}).listen(8080)