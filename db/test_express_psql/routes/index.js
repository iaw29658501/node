var express = require('express');
var router = express.Router();
const db = require('./../database.js');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Super database' });
});

router.get('/list', async function (req, res, next) {
  var results = await db.selectAll('articulo');
  console.log(results);
  res.render('list', { title: 'Express', results: results.rows });
});

router.get('/insert', async function (req, res, next) {
  res.render('insertForm', { title: 'Inserter' });
  maybeInsert(req);
});

router.get('/update', async function (req, res, next) {
  res.render('updateForm', { title: 'You can update the info here' });
  var query = getQuery(req);
  if (db.validUpdate(query)) {
    db.update(query);
  }
})

router.get('/delete', async function (req, res, next) {
  res.render('deleteForm', { title: 'Delete here' });
  let id = getQuery(req).idarticulo;
  if (id !== '' && !isNaN(id)) {
    db.deleteArticulo(id);
  }
})

module.exports = router;
