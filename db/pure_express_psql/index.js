const http = require('http');
const path = require('path');
const db = require('./database.js');
const url = require('url');
const FAVICON = path.join(__dirname, 'public', 'favicon.ico');
//const fs = require('fs');
const express = require('express')
const faker = require('faker')
const bodyParser = require('body-parser')
const expressLayouts = require('express-ejs-layouts')

const app = express()
const port = 8080

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', function (req, res, next) {
    res.render('index', { title: 'Super database' });
});

app.get('/list', async function (req, res, next) {
    var results = await db.selectAll('articulo');
    console.log(results);
    res.render('list', { title: 'Express', results: results.rows });
});

app.get('/insert', async function (req, res, next) {
    res.render('insertForm', { title: 'Inserter' });
    maybeInsert(req);
});

app.get('/update', async function (req, res, next) {
    res.render('updateForm', { title: 'You can update the info here' });
    var query = getQuery(req);
    if (db.validUpdate(query)) {
        db.update(query);
    }
})

app.get('/delete', async function (req, res, next) {
    res.render('deleteForm', { title: 'Delete here' });
    let id = getQuery(req).idarticulo;
    if (id !== '' && !isNaN(id)) {
        db.deleteArticulo(id);
    }
})
app.use(function (req, res) {
    res.write("404")
    res.send()
});


app.listen(port, function (req, res) {
    console.log(`Example app listening on ${port} port!`);
});

function getQuery(req) {
    return url.parse(req.url, true).query;
}

function maybeInsert(req) {
    let query = getQuery(req);
    console.log(query)
    if (Object.keys(query).length == 6) {
        for (var i in query) {
            if (query[i] == null || query[i] == "") {
                return;
            }
        }
        console.log("Got 6 parameters")
        insertArticulo(query);
    }
}

function insertArticulo(query) {
    db.insert(query)
}