const { Client } = require('pg');
const dbConfig = require('./db-config.js');
const client = new Client(dbConfig);

client.connect()
    .then( () => console.log("DB SUCESS -------------------------------------------------"))
    .catch( () => console.log("DB CONNECTION FAILURE ----------------------------------------"));

async function selectAll(table) {
    // TODO
    //sanitize table here
    return await client.query(`SELECT * FROM ${table}`);
}

async function execute(query, params ) {
    // TODO
    //sanitize query here
    return await client.query(query ,params );
}

async function update(query) {
    return await execute("update articulo set codarticulo=$1, descrarticulo=$2, existarticulo=$3, minimoarticulo=$4, preuarticulo=$5, observarticulo=$6 where idarticulo= $7", 
    [query.codarticulo, query.descrarticulo, query.existarticulo, query.minimoarticulo, query.preuarticulo, query.observarticulo, query.idarticulo]);
}

async function insert(params) {
    return await client.query("insert into articulo (codarticulo, descrarticulo, existarticulo, minimoarticulo, preuarticulo, observarticulo) values ($1, $2, $3, $4, $5, $6)", 
    [params.codarticulo, params.descrarticulo, params.existarticulo, params.minimoarticulo, params.preuarticulo, params.observarticulo]);
}

async function deleteArticulo(id) {
    return await client.query("delete from articulo where idarticulo = $1", [id]);
}

async function validUpdate(query) {
    console.log({query})
    let params = [query.codarticulo, query.descrarticulo, query.existarticulo, query.minimoarticulo, query.preuarticulo, query.observarticulo];
    for ( let i in params ) {
        console.log("testing " + params[i])
        if ( params[i] == '' || params[i] == undefined || params[i] == null) {
            console.log(params[i] + " is unaceptable")
            return false;
        }
    }
    return true;
}

function printTable(result, response) {
    response.write("<table  class='table'>")
    //making table header
    response.write("<tr>")
    for (key in result.rows[0]) {
        response.write(`<th>${key}</th>`)
    }
    response.write("</tr>")
    // lets now put the rows
    for ( let i = 0 ; i < result.rows.length; i++) {
        response.write(`<tr>`)
        for ( var key in result.rows[i] ) {
            response.write(`<td> ${result.rows[i][key]} </td>`);
        }
        response.write(`</tr>`);
    }
    response.write("</table>");
}

module.exports = {
    selectAll,
    printTable,
    insert,
    deleteArticulo,
    update,
    validUpdate
}