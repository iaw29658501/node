drop database if exists eduard;

create database eduard;
\c eduard

create user eduard with password 'eduard';

grant select, delete, update, insert on all tables in schema public to eduard;
GRANT ALL PRIVILEGES ON DATABASE eduard TO eduard;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO eduard;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO eduard;

drop table if exists articulo;
create table if not exists articulo (
    idArticulo serial primary key,
    codArticulo varchar(6),
    descrArticulo varchar(40),
    existArticulo integer not null default 0,
    minimoArticulo integer not null default 0,
    preuArticulo decimal(10,2) not null,
    observArticulo text
);

GRANT SELECT, DELETE, UPDATE, INSERT ON ALL TABLES IN SCHEMA PUBLIC TO eduard;
GRANT ALL PRIVILEGES ON DATABASE eduard TO eduard;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO eduard;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO eduard;