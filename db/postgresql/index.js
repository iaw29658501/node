const http = require('http');
const path = require('path');
const db = require('./database.js');
const url = require('url');
const FAVICON = path.join(__dirname, 'public', 'favicon.ico');
const fs = require('fs');


http.createServer(async function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html' });

    var header = fs.readFileSync("header.html");
    response.write(header);
    var pathname = url.parse(request.url).pathname;
    // Routing
    switch (pathname) {
        case '/':
            response.write("<h1>Porfa, execute 'npm run db-init' para inicializar el banco de datos</h1>")
            break;
        case '/insert':
            var insertForm = fs.readFileSync("insertForm.html");
            response.write(insertForm);
            maybeInsert(request);
            break;
        case '/update':
            var updateForm = fs.readFileSync("updateForm.html");
            response.write(updateForm);
           var query = getQuery(request);
            if (db.validUpdate(query)) {
                db.update(query);
            }
            break;
        case '/list':
            var result = await db.selectAll('articulo');
            db.printTable(result, response);
            break;
        case '/delete':
            var deleteForm = fs.readFileSync("deleteForm.html");
            response.write(deleteForm);
            let id = getQuery(request).idarticulo;
            if (id !== '' && !isNaN(id)) {
                db.deleteArticulo(id);
            }
            break;
        default:
            response.write('404');
    }
    response.end("</body></html>");
}).listen(8080);

console.log('Server running at http://127.0.0.1:8080/');


function getQuery(req) {
    return url.parse(req.url, true).query;
}

function maybeInsert(req) {
    let query = getQuery(req);
    console.log(query)
    if (Object.keys(query).length == 6) {
        for (var i in query) {
            if (query[i] == null || query[i] == "") {
                return;
            }
        }
        console.log("Got 6 parameters")
        insertArticulo(query);
    }
}

function insertArticulo(query) {
    db.insert(query)
}