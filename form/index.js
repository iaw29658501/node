var fs = require('fs')
var http = require('http')
var url = require('url')
var util = require('util');
var sanitizer = require('sanitizer')


//TIME BUG FIXED with async function and await 
async function read(file, res) {
    res.write("Lets read " + file);
    var askedFile = await fs.readFile(file)
    res.write("<br>" + sanitizer.escape(askedFile));
}

async function write(file, content, res) {

    res.write("Lets write on " + file);
    await fs.writeFile(file, content, async function (err) {
        if (err) throw err;
    });
}

async function remove(file, res) {
    res.write("Lets remove " + file);
    await fs.unlink(file, (err) => {
        if (err) throw err;
        console.log(file);
      });;
}

fs.readFile = util.promisify(fs.readFile);

http.createServer(async function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/html' });

    var action = getQuery(req).action;

    var form = await fs.readFile('form.html');
    if (!form) {
        res.write("Internal error, no file found");
        res.end();
        return;
    }

    res.write(form.toString());

    /// FORM RESPONSE
    if (action) {
        try {
            // SECURITY RESTRINCTING DANGEROUS CHARACTERS
            res.write("<br>For security measures only numbers and letters are allowed in the file name to restrinc the path from things like (../../file) and from XSS <br>")
            // SECURITY : restrincting file extention
            var file = getQuery(req).file.replace(/^a-z0-9_-./gi, '');
            if (file.substr(file.lastIndexOf('.')) != ".txt") { file += ".txt"; }
            if (action == "read") {
                await read(file, res)
            } else if (action == "write") {
                let content = getQuery(req).content;
                await write(file, content, res)
            } else if (action == "remove") {
                remove(file,res);
            }
        } catch (err) {
            res.write("<br>ERROR FILE NOT FOUND<br>");
            // TODO REMOVE THE NEXT LINE
            res.write(err.toString())
        }
    }
    res.end();
}).listen(8080);



function getQuery(req) {
    return url.parse(req.url, true).query;
}